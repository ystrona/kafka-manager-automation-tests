class AddClusterResultPage(object):

    def check_cluster_already_exists(self):
        return "Cluster already exists" in self.page_source

    def __init__(self, driver):
        self.driver = driver
        self.page_source = driver.page_source
