from selenium import webdriver
from clusters_home_page import ClustersHomePage
import unittest


class KafkaManagerTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        driver = webdriver.Chrome()
        home_page = ClustersHomePage(driver)
        home_page.delete_all_clusters()
        driver.close()

    def setUp(self):
        self.driver = webdriver.Chrome()

    def tearDown(self):
        home_page = ClustersHomePage(self.driver)
        home_page.delete_all_clusters()
        self.driver.close()

    #   Scenario: successfully adding cluster with default optional values
    #
    #   Given I'm on homepage
    #   When I go to add cluster page
    #   And create cluster with only required fields
    #   Then new cluster is active on homepage

    def test_add_cluster(self):
        driver = self.driver

        cluster_name = "ClusterTest100"
        kafka_version = "0.8.2.2"
        zk_host = "192.168.86.15:2181"

        ClustersHomePage(driver)\
            .navigate_to_add_cluster()\
            .add_cluster(cluster_name, zk_host, kafka_version)

        assert ClustersHomePage(driver).check_cluster_is_active(cluster_name)
        assert ClustersHomePage(driver).get_cluster_version(cluster_name) == kafka_version

    #   Scenario: adding cluster with the same name is not allowed
    #
    #   Given I'm on homepage with one cluster already exists
    #   When I go to add cluster page
    #   And create cluster with only required fields
    #   Then I see message that duplicate cluster already exists

    def test_add_duplicated_cluster(self):
        driver = self.driver

        cluster_name = "ClusterTest9"
        zk_host = "192.168.86.15:2181"
        kafka_version = "0.8.2.2"

        ClustersHomePage(driver) \
            .navigate_to_add_cluster() \
            .add_cluster(cluster_name, zk_host, kafka_version)

        assert ClustersHomePage(driver).check_cluster_is_active(cluster_name) == True

        add_cluster_result_page = ClustersHomePage(driver) \
            .navigate_to_add_cluster() \
            .add_cluster(cluster_name, zk_host, kafka_version)

        assert add_cluster_result_page.check_cluster_already_exists()

    #   Scenario: adding cluster with non default values
    #
    #   Given I'm on homepage
    #   When I go to add cluster page
    #   And create cluster with non default broker view update period seconds
    #   Then the new cluster is created and has non default broker view update period seconds value

    def test_add_cluster_non_default_values(self):
        driver = self.driver

        cluster_name = "ClusterTest100"
        kafka_version = "0.8.2.2"
        zk_host = "192.168.86.15:2181"
        broker_view_update_period_seconds = "20"

        ClustersHomePage(driver)\
            .navigate_to_add_cluster()\
            .add_cluster(cluster_name, zk_host, kafka_version, broker_view_update_period_seconds)

        actual_broker_view_update_period_seconds = ClustersHomePage(driver)\
            .navigate_to_modify_page(cluster_name)\
            .get_broker_view_update_period_seconds()

        assert actual_broker_view_update_period_seconds == broker_view_update_period_seconds

    #   Scenario: cluster is not saved in the session
    #
    #   Given I'm on homepage with one cluster already exist
    #   When I create new cluster
    #   And set up new webdriver
    #   Then cluster is displayed in another session

    def test_session_change(self):
        driver = self.driver

        cluster_name = "ClusterSession"
        kafka_version = "0.8.2.2"
        zk_host = "192.168.86.15:2181"

        ClustersHomePage(self.driver)\
            .navigate_to_add_cluster()\
            .add_cluster(cluster_name, zk_host, kafka_version)

        assert ClustersHomePage(driver).check_cluster_is_active(cluster_name)
        driver.close()

        driver = webdriver.Chrome()
        self.driver = driver

        assert ClustersHomePage(driver).check_cluster_is_active(cluster_name)

    #   Scenario: cluster deletion
    #
    #   Given I'm on homepage with one cluster already exists
    #   When I delete cluster from homepage
    #   Then cluster is not displayed on homepage

    def test_delete_cluster(self):
        driver = self.driver

        cluster_name = "ClusterToDelete"
        kafka_version = "0.8.2.2"
        zk_host = "192.168.86.15:2181"

        ClustersHomePage(driver) \
            .navigate_to_add_cluster() \
            .add_cluster(cluster_name, zk_host, kafka_version)

        ClustersHomePage(driver).delete_cluster(cluster_name)
        assert not ClustersHomePage(driver).check_cluster_is_active(cluster_name)


if __name__ == '__main__':
    unittest.main()
