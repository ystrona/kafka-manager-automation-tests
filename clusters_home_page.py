import time
from selenium.common.exceptions import NoSuchElementException
from add_cluster_page import AddClusterPage
from modify_cluster_page import ModifyClusterPage


class ClustersHomePage(object):

    __page_address = "http://localhost:9000"
    __add_cluster_link_text = "Add Cluster"
    __dropdown_class_name = 'dropdown'

    def __cluster_view_link(self, cluster_name):
        return self.driver.find_element_by_link_text(cluster_name)

    @staticmethod
    def __modify_button_xpath(cluster_name):
        return "//tr/td[contains(.,'" + cluster_name + "')]/../td[2]/div/a[@role = 'button']"

    @staticmethod
    def __clusters_list_xpath():
        return "//a[contains(@href, 'clusters')]"

    @staticmethod
    def __submit_button_element_xpath(cluster_name):
        return "//form[@action='/clusters/" + cluster_name + "']//button[@type='submit']"

    @staticmethod
    def __disable_button_xpath(cluster_name):
        return "//form[@action='/clusters/" + cluster_name + "']//button[@type='submit' and contains(.,'Disable')]"

    @staticmethod
    def __delete_button_xpath(cluster_name):
        return "//form[@action='/clusters/" + cluster_name + "']//button[contains(.,'Delete')]"

    @staticmethod
    def __version_element_xpath(cluster_name):
        return "//tr/td[contains(., '" + cluster_name + "')] /../ td[3]"

    @staticmethod
    def __check_exists_by_xpath(xpath, driver):
        try:
            driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def navigate_to_add_cluster(self):
        cluster_dropdown = self.driver.find_element_by_class_name(self.__dropdown_class_name)
        cluster_dropdown.click()
        add_cluster_link = self.driver.find_element_by_link_text(self.__add_cluster_link_text)
        add_cluster_link.click()
        return AddClusterPage(self.driver)

    def navigate_to_cluster_view(self, cluster_name):
        self.__cluster_view_link(cluster_name)

    def navigate_to_modify_page(self, cluster_name):
        modify_button = self.driver.find_element_by_xpath(ClustersHomePage.__modify_button_xpath(cluster_name))
        modify_button.click()
        return ModifyClusterPage(self.driver)

    def get_all_active_clusters_name(self):
        elements = self.driver.find_elements_by_xpath(ClustersHomePage.__clusters_list_xpath())
        clusters_name = []
        for cluster in elements:
            clusters_name.append(cluster.text)
        return clusters_name

    def check_cluster_is_active(self, cluster_name):
        try:
            self.__cluster_view_link(cluster_name)
        except NoSuchElementException:
            return False
        return True

    def __wait_until_cluster_changes_applied(self, cluster_name, max_tries, driver):
        for i in range(0, max_tries):
            if ClustersHomePage.__check_exists_by_xpath(ClustersHomePage.__submit_button_element_xpath(cluster_name), driver):
                return
            else:
                time.sleep(2)
                driver.get(self.__page_address)
        raise ValueError('exception')

    def __disable_cluster(self, cluster_name):
        disable_button = self.driver.find_element_by_xpath(ClustersHomePage.__disable_button_xpath(cluster_name))
        disable_button.click()
        return ClustersHomePage(self.driver)

    def __delete_disabled_cluster(self, cluster_name):
        self.__wait_until_cluster_changes_applied(cluster_name, 5, self.driver)
        delete_button = self.driver.find_element_by_xpath(ClustersHomePage.__delete_button_xpath(cluster_name))
        delete_button.click()
        return ClustersHomePage(self.driver)

    def delete_cluster(self, cluster_name):
        self.__disable_cluster(cluster_name)
        self.__delete_disabled_cluster(cluster_name)
        return ClustersHomePage(self.driver)

    def delete_all_clusters(self):
        home_page = self
        all_clusters = self.get_all_active_clusters_name()
        for cluster in all_clusters:
            home_page = home_page.__disable_cluster(cluster)
            home_page = home_page.__delete_disabled_cluster(cluster)
        return ClustersHomePage(self.driver)

    def get_cluster_version(self, cluster_name):
        version_element = self.driver.find_element_by_xpath(ClustersHomePage.__version_element_xpath(cluster_name))
        return version_element.text

    def __init__(self, driver):
        self.driver = driver
        self.driver.get(self.__page_address)
