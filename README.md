Kafka-manager Automation Tests
====================

This project contains some of the automated Selenium tests for [Kafka-manager](https://github.com/yahoo/kafka-manager).

All tests can be run from `KafkaManagerTest` class inside `tests.py` file.

In order to run tests you need a started Kafka-manager application listening on 0.0.0.0:9000.