
class ModifyClusterPage(object):
    __broker_view_update_period_seconds = "tuning_brokerViewUpdatePeriodSeconds"

    def get_broker_view_update_period_seconds(self):
        return self.broker_view_update_period_seconds_elem.get_attribute("value")

    def __init__(self, driver):
        self.driver = driver
        self.broker_view_update_period_seconds_elem = self.driver.find_element_by_id(
            self.__broker_view_update_period_seconds)