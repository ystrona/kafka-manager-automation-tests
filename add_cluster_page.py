from selenium.webdriver.support.ui import Select
from add_cluster_result_page import AddClusterResultPage


class AddClusterPage(object):
    __cluster_name_field = "name"
    __zk_host_field = "zkHosts"
    __kafka_version = "kafkaVersion"
    __broker_view_update_period_seconds = "tuning_brokerViewUpdatePeriodSeconds"
    __save_button_element = "//button[@type='submit']"

    def add_cluster(self, cluster_name, zk_host, kafka_version, broker_view_update_period_seconds=30):
        self.cluster_name_field.send_keys(cluster_name)
        self.zk_host_field.send_keys(zk_host)
        self.kafka_version.select_by_value(kafka_version)
        self.broker_view_update_period_seconds.clear()
        self.broker_view_update_period_seconds.send_keys(broker_view_update_period_seconds)
        self.save_button_element.click()
        return AddClusterResultPage(self.driver)

    def __init__(self, driver):
        self.driver = driver
        self.cluster_name_field = self.driver.find_element_by_name(self.__cluster_name_field)
        self.zk_host_field = self.driver.find_element_by_name(self.__zk_host_field)
        self.kafka_version = Select(self.driver.find_element_by_id(self.__kafka_version))
        self.broker_view_update_period_seconds = self.driver.find_element_by_id(self.__broker_view_update_period_seconds)
        self.save_button_element = self.driver.find_element_by_xpath(self.__save_button_element)

